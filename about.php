
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Document Title -->
        <title>GMRIT Library Management| About</title>

        <!-- StyleSheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">



        <!-- JavaScripts -->
        <style>.panel {
                margin-bottom: 20px;
                background-color: #fff;
                border: 1px solid #F2F3F3;
            }

            *{margin:0px}
            #searchbox
            {
            }
            #display
            {
                width:250px;
                display:none;
                float:right; margin-right:60px;
                border-left:solid 1px #dedede;
                border-right:solid 1px #dedede;
                border-bottom:solid 1px #dedede;
                overflow:hidden;
            }
            .display_box
            {
                color:#222;
                border-top:solid 1px #dedede; 
                font-size:12px; 
                height:30px;
                overflow:hidden;
            }
            .display_box:hover
            {
                background-color:#284761;
                color:#FFF;
            }
        </style>

    </head>

    <body>

        <!-- Wrapper -->
        <div class="wrapper push-wrapper">

            <!-- Header -->
            <header id="header">

                <!-- Top Bar -->
                <div class="topbar">
                    <div class="container header-cont">

                        <div class="col-md-4 logo">
                            <a style="display: inline-flex;" href="index.php"><img src="img/logo.png" class="img-logo"><H1 class="LogoText">IT </H1><H1 style="color:orange !important;    font-size: 29px;margin-top: 16px;" class="LogoText">LMS </H1></a>
                        </div>
                        <div class="col-md-8 ">
                            <ul class="options">
                                <li class="option">
                                    <a href="index.php" style="color:#FFFFFF">
                                        <i class="fa fa-home"></i>&nbsp;Home</a>
                                </li>
                                <li class="option">
                                    <a href="about.php" style="color:#FFFFFF">
                                        <i class="fa fa fa-files-o"></i>&nbsp;About</a>
                                </li>
                                <li class="option">
                                    <a href="login.php" style="color:#FFFFFF">
                                        <i class="fa fa-sign-in"></i>&nbsp;SignIn</a>
                                </li>
                                <li class="option">
                                    <a  href="signup.php"style="color:#FFFFFF">
                                        <i class="fa fa-sign-out"></i></span>&nbsp;signUp</a>
                                </li>
                            </ul>

                        </div>

                    </div>
                </div>
                <!-- Top Bar -->


            </header>
            <!-- Header -->

            <!-- Main Content -->
            <main class="main-content" style="background:#fff">

                <!-- Book Collections -->
                <section class="book-collection">
                    <div class="container">
                        <div class="row">



                        </div>
                    </div>
                </section>

                <br/>

            </main>
            <!-- Main Content -->

            <!-- Footer -->
            <footer id="footer"> 



                <!-- Sub Footer -->
                <div class="sub-foorer">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>Copyright <i class="fa fa-copyright"></i> 2018 <span class="theme-color"></span> All Rights Reserved.</p>
                            </div>
                            <div class="col-sm-6">
                                <a class="back-top" href="#">Back to Top<i class="fa fa-caret-up"></i></a>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Sub Footer -->

            </footer>
            <!-- Footer -->

        </div>

    </body>

</html>