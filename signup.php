
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Document Title -->
        <title>GMRIT Library Management| SignUp</title>

        <!-- StyleSheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">



        <!-- JavaScripts -->
        <style>.panel {
                margin-bottom: 20px;
                background-color: #fff;
                border: 1px solid #F2F3F3;
            }

            *{margin:0px}
            #searchbox
            {
            }
            #display
            {
                width:250px;
                display:none;
                float:right; margin-right:60px;
                border-left:solid 1px #dedede;
                border-right:solid 1px #dedede;
                border-bottom:solid 1px #dedede;
                overflow:hidden;
            }
            .display_box
            {
                color:#222;
                border-top:solid 1px #dedede; 
                font-size:12px; 
                height:30px;
                overflow:hidden;
            }
            .display_box:hover
            {
                background-color:#284761;
                color:#FFF;
            }

            .book-collection {
                padding:50px 0px;
            }

            .login-wrap{
                width:100%;
                margin:auto;
                max-width: 360px;
                min-height: 635px;
                position:relative;
                background:url(https://www.sciencenewsforstudents.org/sites/default/files/2017/05/main/blogposts/860_main_library_bacteria.png) no-repeat center;
                box-shadow:0 12px 15px 0 rgba(0,0,0,.24),0 17px 50px 0 rgba(0,0,0,.19);
            }
            .login-html{
                width:100%;
                height:100%;
                position:absolute;
                padding: 30px 30px 30px 30px;
                background: rgba(40,57,101,.6);
            }
            .login-html .sign-in-htm,
            .login-html .sign-up-htm{
                top:0;
                left:0;
                right:0;
                bottom:0;
                position:absolute;
                transform:rotateY(180deg);
                backface-visibility:hidden;
                transition:all .4s linear;
            }
            .login-html .sign-in,
            .login-html .sign-up,
            .login-form .group .check{
                display:none;
            }
            .login-html .tab,
            .login-form .group .label,
            .login-form .group .button{
                text-transform:uppercase;
            }
            .login-html .tab{
                font-size:22px;
                margin-right:15px;
                padding-bottom:5px;
                margin:0 15px 10px 0;
                display:inline-block;
                border-bottom:2px solid transparent;
            }
            .login-html .sign-in:checked + .tab,
            .login-html .sign-up:checked + .tab{
                color:#fff;
                border-color:#ff9c3c;
                padding: 0px 90px 0px 90px;
                margin-left: 20px;
            }
            .login-form{
                min-height:345px;
                position:relative;
                perspective:1000px;
                transform-style:preserve-3d;
            }
            .login-form .group{
                margin-bottom:15px;
            }
            .login-form .group .label,
            .login-form .group .input,
            .login-form .group .button{
                width:100%;
                color:#fff;
                display:block;
            }
            .login-form .group .input,
            .login-form .group .button{
                border:none;
                border-radius:25px;
                background:rgba(255,255,255,.1);
            }
            .login-form .group input[data-type="password"]{
                text-security:circle;
                -webkit-text-security:circle;
            }
            .login-form .group .label{
                color:#aaa;
                font-size:12px;
            }
            .login-form .group .button{
                background:#ff9c3c;
            }
            .login-form .group label .icon{
                width:15px;
                height:15px;
                border-radius:2px;
                position:relative;
                display:inline-block;
                background:rgba(255,255,255,.1);
            }
            .login-form .group label .icon:before,
            .login-form .group label .icon:after{
                content:'';
                width:10px;
                height:2px;
                background:#fff;
                position:absolute;
                transition:all .2s ease-in-out 0s;
            }
            .login-form .group label .icon:before{
                left:3px;
                width:5px;
                bottom:6px;
                transform:scale(0) rotate(0);
            }
            .login-form .group label .icon:after{
                top:6px;
                right:0;
                transform:scale(0) rotate(0);
            }
            .login-form .group .check:checked + label{
                color:#fff;
            }
            .login-form .group .check:checked + label .icon{
                background:#ff9c3c;
            }
            .login-form .group .check:checked + label .icon:before{
                transform:scale(1) rotate(45deg);
            }
            .login-form .group .check:checked + label .icon:after{
                transform:scale(1) rotate(-45deg);
            }
            .login-html .sign-in:checked + .tab + .sign-up + .tab + .login-form .sign-in-htm{
                transform:rotate(0);
            }
            .login-html .sign-up:checked + .tab + .login-form .sign-up-htm{
                transform:rotate(0);
            }

            .hr{
                height:2px;
                margin:30px 0 30px 0;
                background:rgba(255,255,255,.2);
            }
            .foot-lnk{
                text-align:center;
            }
        </style>

    </head>

    <body>

        <!-- Wrapper -->
        <div class="wrapper push-wrapper">

            <!-- Header -->
            <header id="header">

                <!-- Top Bar -->
                <div class="topbar">
                    <div class="container header-cont">

                        <div class="col-md-4 logo">
                            <a href="index.php"><H1 style="color:ORANGE;font-size:50px;font-weight:bold">LMS</H1></a>
                        </div>
                        <div class="col-md-8 ">
                            <ul class="options">
                                <li class="option">
                                    <a href="index.php" style="color:#FFFFFF">
                                        <i class="fa fa-home"></i>&nbsp;Home</a>
                                </li>
                                <li class="option">
                                    <a href="about.php" style="color:#FFFFFF">
                                        <i class="fa fa fa-files-o"></i>&nbsp;About</a>
                                </li>
                                <li class="option">
                                    <a href="login.php" style="color:#FFFFFF">
                                        <i class="fa fa-sign-in"></i>&nbsp;SignIn</a>
                                </li>
                                <li class="option">
                                    <a  href="signup.php"style="color:#FFFFFF">
                                        <i class="fa fa-sign-out"></i></span>&nbsp;signUp</a>
                                </li>
                            </ul>

                        </div>

                    </div>
                </div>
                <!-- Top Bar -->


            </header>
            <!-- Header -->

            <!-- Main Content -->
            <main class="main-content" style="background:#fff">

                <!-- Book Collections -->
                <section class="book-collection">

                    <div class="login-wrap">
                        <div class="login-html">
                            <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign Up</label>
                            <input id="tab-2" type="radio" name="tab" class="sign-up"><label style="display:none !important" for="tab-2" class="tab">Sign Up</label>
                            <div class="login-form">
                                <div class="sign-in-htm">
                                    <div class="group">
                                        <label for="user" class="label">Name</label>
                                        <input name="name" id="name" type="text" class="input">
                                    </div>
                                    <div class="group">
                                        <label for="user" class="label">Admission No</label>
                                        <input name="jntu" id="student-id" type="text" class="input">
                                    </div>
                                    <div class="group">
                                        <label for="user" class="label">Email Address</label>
                                        <input name="email" id="email" type="text" class="input">
                                    </div>
                                    <div class="group">
                                        <label for="user" class="label">Mobile No</label>
                                        <input name="mobile" id="mobile" type="text" class="input">
                                    </div>
                                    <div class="group">
                                        <label for="pass" class="label">Password</label>
                                        <input name="pass" id="pass" type="password" class="input" data-type="password">
                                    </div>
                                    <div class="group">
                                        <label for="pass" class="label">Repeat Password</label>
                                        <input name="cpass" id="cpass" type="password" class="input" data-type="password">
                                    </div>
                                    
                                    <div class="group">
                                        <input type="submit" class="button" value="Sign Up">
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <br/>

            </main>
            <!-- Main Content -->

            <!-- Footer -->
            <footer id="footer"> 



                <!-- Sub Footer -->
                <div class="sub-foorer">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>Copyright <i class="fa fa-copyright"></i> 2018 <span class="theme-color"></span> All Rights Reserved.</p>
                            </div>
                            <div class="col-sm-6">
                                <a class="back-top" href="#">Back to Top<i class="fa fa-caret-up"></i></a>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Sub Footer -->

            </footer>
            <!-- Footer -->

        </div>
		<script>
            function sugnUp() {
				var name = $('#name').val();
				var student_id = $('#student_id').val();
				var email = $('#email').val();
				var mobile = $('#mobile').val();
				var pass = $('#pass').val();
				var cpass = $('#cpass').val();
                $.ajax({
                    type: "POST",
                    url: "student-signup.php",
                    data: data,
                    success: function (dataString) {
                        $('#sub-dashboard').html(dataString);
                    }
                });
            }
        </script>
    </body>

</html>