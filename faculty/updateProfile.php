<?php
include_once 'session.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!empty($_POST)) {
    $user_id = $_SESSION['faculty']['faculty_id'];
    $query = "UPDATE `faculty` SET `faculty_id`='$user_id'";

    if (isset($_POST['email'])) {
        $email = xssFilter($_POST['email']);
        $query .= ",`email`='$email'";
    }
    if (isset($_POST['mobile'])) {
        $mobile = xssFilter($_POST['mobile']);
        $query .= ",`mobile`='$mobile'";
    }
    if (isset($_POST['pass'])) {
        $pass = aes(xssFilter($_POST['pass']), 'encrypt');
        $query .= ",`password`='$pass'";
    }
    $query .= "WHERE `faculty_id` = '$user_id'";

    $query = exeQuery($query);
    if ($query == true) {
        ?>
        <div class="alert alert-success">
            <strong>Success!</strong>Profile Updated Successfully.
        </div>
        <style>
            #alert-data{
                margin:100px;
            }
        </style>
        <?php
    } else {
        ?>
        <div class="alert alert-danger">
            <strong>Danger!</strong>Something Went Wrong Try Again Later.
        </div>
        <style>
            #alert-data{
                margin:100px;
            }
        </style>
        <?php
    }
}

