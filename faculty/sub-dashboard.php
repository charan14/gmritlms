<?php
include_once 'session.php';

if (isset($_POST['action'])) {

    switch ($_POST['action']) {
        case "search-book":
            ?>
            <script>
                $('#li-dashboard').attr('class', '');
                $('#li-Update-profile').attr('class', '');
                $('#li-search-book').attr('class', 'active');
            </script>
            <div class = "container">
                <center>
                    <div style="margin:100px" class = "col-lg-8">
                        <input style="border-radius: 12px;outline: none;color: black;" name="book-name" id="book-name" type="text" autofocus />
                        <button onclick="Search()" style="margin:20px" type="button" class="btn btn-info">Search</button>
                    </div>
                </center>
            </div>
            <script>
                function Search() {
                    var search = $("#book-name").val();
                    var data = "search=" + search;
                    //alert(search);
					if(search.length >=5){
                    $.ajax({
                        type: "POST",
                        url: "book-search.php",
                        data: data,
                        success: function (dataString) {
                            //alert(dataString);
                            $('#sub-dashboard').html(dataString);
                        }
                    });
					}else{
						alert("Keyword should have minimum 5 characters");
					}
                }
            </script>
            <?php
            break;
        case "books":
            $Books = runQuery("SELECT * FROM `books`");
            ?>
            <script>
                $('#li-dashboard').attr('class', '');
                $('#li-Update-profile').attr('class', '');
                $('#li-search-book').attr('class', '');
            </script>
            </script>
            <div class = "container">
                <center>
                    <div style="margin:50px" class = "col-lg-8">
                        <table border="1px">
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Author</th>
                                <th>Publisher</th>
                                <th>Pdf</th>
                            </tr>
                            <?php
                            foreach ($Books as $key => $value) {
                                ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $Books[$key]['book_name']; ?></td>
                                    <td><?php echo $Books[$key]['category']; ?></td>
                                    <td><?php echo $Books[$key]['author']; ?></td>
                                    <td><?php echo $Books[$key]['publisher']; ?></td>
                                    <td><a href="<?php echo $Books[$key]['url']; ?>" target="_blank"><?php echo $Books[$key]['book_name'] . "By" . $Books[$key]['author']; ?></a></td>

                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                </center>
            </div>

            <?php
            break;
        
        case "dashboard":
            
            ?>
            <div class="container">
                <center>
                    <div class="col-lg-8">
                        <h2 class="welcome-name">Welcome:<?php echo $_SESSION['faculty']['name']; ?></h2>
                </center>
                <style>
                    .welcome-name{
                        margin-top: 50px;
                        margin-left: -363px;
                    }
                </style>
            </div>
            <script>
                $('#li-dashboard').attr('class', 'active');
                $('#li-Update-profile').attr('class', '');
                $('#li-search-book').attr('class', '');

            </script>
            <?php
            break;
		case "update-profile":
		
            $user_data = runQuery("SELECT * FROM `faculty` WHERE `faculty_id` = '$user_id'");
			// var_dump($user_data);
            ?>
            <div class="container">
                <div class="col-lg-12">
                    <h2 class="welcome-name">Update Profile</h2>
                    <div class="col-md-8" id="alert-data">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="user_date"><label class="user-label">Name</label><input class="input" id="name" type="text" value="<?php echo $user_data[0]['name']; ?>" readonly></div>
                                <div class="user_date"><label class="user-label">Faculty Id</label><input class="input" id="student_id" type="text" value="<?php echo $user_data[0]['faculty_id']; ?>" readonly></div>
                                <div class="user_date"><label class="user-label">Department</label><input class="input" id="dept" type="text" value="<?php echo $user_data[0]['dept']; ?>" readonly></div>
                                <div class="user_date"><label class="user-label">Enter Password</label><input class="input" id="pass" type="password" required></div>
                            </div>
                            <div class="col-md-5">
                                <div class="user_date"><label class="user-label">Email Id</label><input class="input" id="email" type="text" value="<?php echo $user_data[0]['email']; ?>" ></div>
                                <div class="user_date"><label class="user-label">Phone No</label><input class="input" id="phone" type="text" value="<?php echo $user_data[0]['mobile']; ?>" ></div>
                                <div class="user_date"><label class="user-label">Designation</label><input class="input" id="dob" type="text" value="<?php echo $user_data[0]['post']; ?>" readonly></div>
                                <div class="user_date"><label class="user-label">Confirm Password</label><input class="input" id="cpass" type="password" required></div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <button onclick="updateProfile()" style="margin:20px;float:right;margin-right:250px;" type="button" class="btn btn-info">Update</button>
                        </div>
                        <style>
                            .welcome-name{
                                margin-top: 50px;
                                margin-left: -363px;
                                text-align: center;
                            }
                            .user_date{
                                display:inline-flex;
                                max-width: 340px;
                                margin:5px;
                            }
                            label.user-label {
                                width: 150px;
                                margin-top: 14px;
                            }
                            .input{
                                border-radius: 10px;
                            }
                        </style>
                    </div>
                </div>
                <script>
                    $('#li-dashboard').attr('class', '');
                    $('#li-search-book').attr('class', '');
                    $('#li-reserved-books').attr('class', '');
                    $('#li-check-notification').attr('class', '');
                    $('#li-update-profile').attr('class', 'active');

                    function updateProfile() {
                        var email = $('#email').val();
                        var mobile = $('#phone').val();
                        var dob = $('#dob').val();
                        var pass = $('#pass').val();
                        var cpass = $('#cpass').val();

                        if (pass == cpass) {
                            var data = "email=" + email + "&mobile=" + mobile + "&dob=" + dob + "&pass=" + pass;
                            alert(data);
                            $.ajax({
                                type: "POST",
                                url: "updateProfile.php",
                                data: data,
                                success: function (dataString) {
                                    alert(dataString);
                                    $('#alert-data').html(dataString);
                                }
                            });
                        } else {
                            alert('password and confirm password not matched');
                        }
                    }

                </script>
                <?php
                break;
        default:
    }
}
?>