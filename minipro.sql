-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2018 at 08:04 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minipro`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `id` text NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `mobile` text NOT NULL,
  `pass` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `id`, `name`, `email`, `mobile`, `pass`) VALUES
(8, '15341a05d7', 'Sri Charan', 'charan.saithana@gmail.com', '9951448589', 'jem8avFrYztp8b9gLawqDw=='),
(9, '15341A05F2', 'S.Pranitha', 'pranithasoppa@gmail.com', '9866289905', 'oBN3Jh3SCWOYs62B8Ix8Zw=='),
(11, '123654', 'pavan', 'pavansai@gmail.com', '9517538524', '9aeNVyaIGbRfPMEn/e9ltw==');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `code` text NOT NULL,
  `book_name` text NOT NULL,
  `category` text NOT NULL,
  `count` text NOT NULL,
  `author` text NOT NULL,
  `publisher` text NOT NULL,
  `rating` int(11) NOT NULL,
  `url` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `code`, `book_name`, `category`, `count`, `author`, `publisher`, `rating`, `url`, `image`) VALUES
(1, 'cse0001', 'Introduction to Algorithms ', 'cse', '4', 'Thomas H. Cormen ', 'MIT Press', 4, 'http://ressources.unisciel.fr/algoprog/s00aaroot/aa00module1/res/%5BCormen-AL2011%5DIntroduction_To_Algorithms-A3.pdf', 'https://rukminim1.flixcart.com/image/832/832/book/8/4/8/introduction-to-algorithms-original-imadddghtzxgsh4t.jpeg'),
(2, 'cse0002', 'Structure and Interpretation of Computer Programs', 'cse', '6', 'Hal Abelson, Gerald Jay Sussman', 'MIT Press', 3, 'https://mitpress.mit.edu/sites/default/files/6515.pdf', 'https://images-na.ssl-images-amazon.com/images/I/51H17R%2BbW8L._SX331_BO1,204,203,200_.jpg'),
(3, 'cse0003', 'The C Programming Language', 'cse', '5', 'Brian Kernighan, Dennis Ritchie', '', 5, 'http://cs.indstate.edu/~cbasavaraj/cs559/the_c_programming_language_2.pdf', 'https://images-na.ssl-images-amazon.com/images/I/41gHB8KelXL._SX377_BO1,204,203,200_.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `faculty_id` text NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `mobile` text NOT NULL,
  `password` text NOT NULL,
  `dept` text NOT NULL,
  `qualification` text NOT NULL,
  `post` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `faculty_id`, `name`, `email`, `mobile`, `password`, `dept`, `qualification`, `post`) VALUES
(1, '5d7', 'Sri Charan', 'charan.saithana@gmail.com', '9951448589', 'jem8avFrYztp8b9gLawqDw==', 'cse', 'm.tech', 'asst prof'),
(2, '50718', 'chakradhar', 'chakri.gmrit@gmail.com', '7702215245', 'QoXrOA0j/zJK+Fk0MLrA7A==', 'cse', 'm.tech', 'asst. prof'),
(7, '123456', 'pranitha.s', 'pranithasoppa@gmail.com', '9876543214', 'oBN3Jh3SCWOYs62B8Ix8Zw==', 'CSE', 'Ph.D', 'Prof');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `mobile` text NOT NULL,
  `profilePic` text NOT NULL,
  `imageurl` text NOT NULL,
  `dob` text NOT NULL,
  `gender` text NOT NULL,
  `student_id` text NOT NULL,
  `branch` text NOT NULL,
  `year` text NOT NULL,
  `password` text NOT NULL,
  `os` text NOT NULL,
  `mac` text NOT NULL,
  `ip` text NOT NULL,
  `browser` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `email`, `mobile`, `profilePic`, `imageurl`, `dob`, `gender`, `student_id`, `branch`, `year`, `password`, `os`, `mac`, `ip`, `browser`) VALUES
(1, 'Sricharan', 'charan.saithana@gmail.com', '9951448589', '', '', '1997-05-14', 'M', '15341A05D7', 'CSE', '3', 'jem8avFrYztp8b9gLawqDw==', 'Windows 10', '54-53-ED-B7-A6-C5', '::1', 'Chrome'),
(3, 'pranitha .s', 'pranithasoppa@gmail.com', '9866289905', '', '', '19-06-1997', 'female', '15341A05F2', 'CSE', '2015-2019', 'oBN3Jh3SCWOYs62B8Ix8Zw==', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_book_reserve`
--

CREATE TABLE `user_book_reserve` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `book_code` text NOT NULL,
  `reserve_date` text NOT NULL,
  `last_date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_book_reserve`
--

INSERT INTO `user_book_reserve` (`id`, `user_id`, `book_code`, `reserve_date`, `last_date`) VALUES
(3, '15341A05D7', 'cse0002', '19-03-2018', '20-03-2018'),
(5, '15341A05D7', 'cse0003', '19-03-2018', '20-03-2018');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_book_reserve`
--
ALTER TABLE `user_book_reserve`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_book_reserve`
--
ALTER TABLE `user_book_reserve`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
