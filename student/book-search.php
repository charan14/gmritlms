<?php
include_once 'session.php';

if (isset($_POST['search'])) {
    $search = xssFilter($_POST['search']);

    $result = runQuery("SELECT * FROM `books` WHERE book_name like '%$search%'");
    //var_dump($result);
    ?>
    <link href="../css/font-awesome.min.css" rel="stylesheet" >
    <style>
        body{
            overflow:hidden;
        }
        .card {
            width: 250px;
            display: inline-block;
            font-size: 1em;
            overflow: hidden;
            padding: 0;
            border: none;
            border-radius: .28571429rem;
            box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
        }

        .card-block {
            font-size: 1em;
            position: relative;
            margin: 0;
            padding: 1em;
            border: none;
            border-top: 1px solid rgba(34, 36, 38, .1);
            box-shadow: none;
        }

        .card-img-top {
            display: block;
            width: 100%;
            height: auto;
        }

        .card-title {
            font-size: 1.28571429em;
            font-weight: 700;
            line-height: 1.2857em;
        }

        .card-text {
            clear: both;
            margin-top: .5em;
            color: rgba(0, 0, 0, .68);
        }

        .card-footer {
            font-size: 1em;
            position: static;
            top: 0;
            left: 0;
            max-width: 100%;

            color: rgba(0, 0, 0, .4);
            border-top: 1px solid rgba(0, 0, 0, .05) !important;
            background: #fff;
        }

        .card-inverse .btn {
            border: 1px solid rgba(0, 0, 0, .05);
        }

        .profile {
            position: absolute;
            top: -12px;
            display: inline-block;
            overflow: hidden;
            box-sizing: border-box;
            width: 25px;
            height: 25px;
            margin: 0;
            border: 1px solid #fff;
            border-radius: 50%;
        }

        .profile-avatar {
            display: block;
            width: 100%;
            height: auto;
            border-radius: 50%;
        }

        .profile-inline {
            position: relative;
            top: 0;
            display: inline-block;
        }

        .profile-inline ~ .card-title {
            display: inline-block;
            margin-left: 4px;
            vertical-align: top;
        }

        .text-bold {
            font-weight: 700;
        }

        .meta {
            font-size: 1em;
            color: rgba(0, 0, 0, .4);
        }
        .profile_budget{
            padding-top:15px;
            color:#757575;
        }

        .meta h5 {
            text-decoration: none;

            font-weight:normal;
        }
        .card-footer .icon{
            padding:10px 10px;
        }
        .card-footer .icon a{
            color:#ef6645;
        }
        .scroll{
            overflow-y: scroll;
            height: 74vh;
            width: 100%;
        }
        .books-div.col-md-10 {
            border: 1px solid #c5c5c5;
            margin: 10px;
            padding: 10px;
        }
        .checked {
            color: orange;
        }
    </style>
    <div class="container">
        <center>
            <div style="margin:10px;display: flex;" class = "col-lg-11">
                <input style="border-radius: 12px;outline: none;color: black;" name="book-name" id="book-name" type="text" autofocus />
                <button onclick="Search()" style="margin:20px;margin-top: 9px;" type="button" class="btn btn-info">Search</button>
            </div>
            <hr>
        </center>
        <script>
            function Search() {
                var search = $("#book-name").val();
                var data = "search=" + search;
                //alert(search);

                if (search.length >= 3) {
                    $.ajax({
                        type: "POST",
                        url: "book-search.php",
                        data: data,
                        success: function (dataString) {
                            //alert(dataString);
                            $('#sub-dashboard').html(dataString);
                        }
                    });
                } else {
                    alert("Keyword should have minimum 3 characters");
                }
            }
            function StudentReserve(code, token) {
                var data = "code=" + code + "&token=" + token;
                $.ajax({
                    type: "POST",
                    url: "reserveBook.php",
                    data: data,
                    success: function (dataString) {
                        document.getElementById("reserve_" + code).innerHTML = dataString;
                    }
                });
            }
        </script>
        <div class="container">
            <div class="scroll col-md-3 books">
                <?php
                if (!empty($result)) {
                    $_SESSION['reserveToken'] = token(10);
                    foreach ($result as $key => $value) {
                        ?>
                        <div class="books-div col-md-10">
                            <div class="col-md-2">
                                <img src="<?php echo $result[$key]['image']; ?>">
                            </div>
                            <div class="col-md-10">
                                <h3 class="card-title mt-3"><?php echo $result[$key]['book_name']; ?></h3>
                                <h4 class="book-author"><?php echo $result[$key]['author']; ?></h4>
                                <h5><?php echo $result[$key]['publisher']; ?> Publication</h5>

                                <?php
                                //echo $result[$key]['rating'];
                                for ($i = 1; $i <= 5; $i++) {
                                    if ($i <= $result[$key]['rating']) {
                                        ?>
                                        <span class="fa fa-star checked"></span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="fa fa-star"></span>
                                        <?php
                                    }
                                }
                                $addDate = ForwardDate(1);
                                $checkBook = runQuery("SELECT `book_code` FROM `user_book_reserve` WHERE user_id = '$user_id' AND last_date >= '$addDate'");
                                //var_dump($result[$key]['code']);
                                foreach ($checkBook as $key1 => $value1) {
                                    $codes[$key1] = $checkBook[$key1]['book_code'];
                                }
                                if (in_array($result[$key]['code'], $codes)) {
                                    ?>
                                    <h5 id="reserve_<?php echo $result[$key]['code'] ?>" style="cursor: pointer;margin:5px;">Reserved</h5>
                                    <?php
                                } else {
                                    ?>         
                                    <h5 id="reserve_<?php echo $result[$key]['code'] ?>" style="cursor: pointer;margin:5px;" onclick="StudentReserve('<?php echo $result[$key]['code'] ?>', '<?php echo $_SESSION['reserveToken']; ?>')">Reserve This Book</h5>
                                    <?php
                                }
                                ?>
                                <div class="icon pull-right">
                                    <a href="<?php echo $result[$key]['url']; ?>"><i class="fa fa-download fa-fw fa-2x" aria-hidden="true"></i></a>
                                    <!--<a href="#"><i class="fa fa-ban fa-2x" aria-hidden="true"></i></a>-->

                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    echo "<h4>No Result Found For <strong>$search</strong></h4>";
                }
                ?>
                <div class="col-md-10" style="height:100px;">
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
