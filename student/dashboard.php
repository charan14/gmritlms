<?php
include_once 'session.php';
?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Document Title -->
        <title>GMRIT Library Management</title>

        <!-- StyleSheets -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/responsive.css">

        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>

        <style>
            #footer{
                position:fixed;
                bottom:0;
            }
            body{
                min-height: 100vh;
                height: 100%;
            }
            .nav-side-menu.col-md-3 {
                padding: 0px;
                height: 80vh;
                background-color:#1e293d;
            }
        </style>

    </head>

    <body>

        <!-- Wrapper -->
        <div class="wrapper push-wrapper">

            <!-- Header -->
            <header id="header">

                <!-- Top Bar -->
                <div class="topbar">
                    <div class="container header-cont">

                        <div class="col-md-4 logo">
                            <a style="display: inline-flex;" href="index.php"><img src="../img/logo.png" class="img-logo"><H1 class="LogoText">IT </H1><H1 style="color:orange !important;    font-size: 29px;margin-top: 16px;" class="LogoText">LMS </H1></a>
                        </div>
                        <div class="col-md-8 ">
                            <ul class="options">
                                <li class="option">
                                    <a href="index.php" style="color:#FFFFFF" data-toggle="tooltip" data-placement="bottom" title="<?php echo $_SESSION['student']['name']; ?>">
                                        <i class="fa fa-user"></i>&nbsp;Student</a>
                                </li>
                                <li class="option">
                                    <a  href="logout.php"style="color:#FFFFFF">
                                        <i class="fa fa-sign-out"></i></span>&nbsp;Logout</a>
                                </li>
                            </ul>

                        </div>

                    </div>
                </div>
                <!-- Top Bar -->


            </header>
            <!-- Header -->

            <!-- Main Content -->
            <main class="main-content" style="background:#fff">
                <div class="nav-side-menu col-md-3">
                    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

                    <div class="menu-list">

                        <ul id="menu-content" class="menu-content collapse out">
                            <li id="li-dashboard" style="border-top: 1px solid #c5c5c5;">
                                <a href="" onclick="actions('dashboard')">
                                    <i class="fa fa-dashboard fa-lg"></i> Dashboard
                                </a>
                            </li>
                            

                            <li id="li-search-book">
                                <a href="#" onclick="actions('search-book')">
                                    <i class="fa fa-search fa-lg"></i>Search Book
                                </a>
                            </li>
                            <li id="li-reserved-books">
                                <a href="#" onclick="actions('reserved-books')"><i class="fa fa-book fa-lg"></i>Reserved Books</a>
                            </li>
                            <!--                            <ul class="sub-menu collapse" id="products">
                                                            <li class="active"><a href="#">Books</a></li>
                                                            <li><a href="#">General</a></li>
                                                            <li><a href="#">Buttons</a></li>
                                                            <li><a href="#">Tabs & Accordions</a></li>
                                                            <li><a href="#">Typography</a></li>
                                                            <li><a href="#">FontAwesome</a></li>
                                                            <li><a href="#">Slider</a></li>
                                                            <li><a href="#">Panels</a></li>
                                                            <li><a href="#">Widgets</a></li>
                                                            <li><a href="#">Bootstrap Model</a></li>
                                                        </ul>-->


                            <li id="li-check-notification">
                                <a href="#" onclick="actions('check-notification')"><i class="fa fa-bell fa-lg"></i>Check Notification</a>
                            </li>  
                            <li id="li-update-profile">
                                <a href="#" onclick="actions('update-profile')"><i class="fa fa-user-circle-o fa-lg"></i>Update Profile</a>
                            </li>  

                        </ul>
                    </div>
                </div>
                <div id="sub-dashboard" class="col-md-9">

                </div>
            </main>
            <!-- Main Content -->

            <!-- Footer -->
            <footer id="footer"> 



                <!-- Sub Footer -->
                <div class="sub-foorer">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>Copyright <i class="fa fa-copyright"></i> 2018 <span class="theme-color"></span> All Rights Reserved.</p>
                            </div>
                            <div class="col-sm-6">
                                <a class="back-top" href="#">Back to Top<i class="fa fa-caret-up"></i></a>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Sub Footer -->

            </footer>
            <!-- Footer -->

        </div>

        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <script>
            function actions(page) {
                var data = "action=" + page;
                $.ajax({
                    type: "POST",
                    url: "sub-dashboard.php",
                    data: data,
                    success: function (dataString) {
                        $('#sub-dashboard').html(dataString);
                    }
                });
            }
            actions('dashboard');
        </script>

    </body>

</html>