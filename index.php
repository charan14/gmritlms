
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Document Title -->
        <title>GMRIT Library Management</title>

        <!-- StyleSheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <!-- JavaScripts -->
        <style>.panel {
                margin-bottom: 20px;
                background-color: #fff;
                border: 1px solid #F2F3F3;
            }

            *{margin:0px}
            #searchbox
            {
            }
            #display
            {
                width:250px;
                display:none;
                float:right; margin-right:60px;
                border-left:solid 1px #dedede;
                border-right:solid 1px #dedede;
                border-bottom:solid 1px #dedede;
                overflow:hidden;
            }
            .display_box
            {
                color:#222;
                border-top:solid 1px #dedede; 
                font-size:12px; 
                height:30px;
                overflow:hidden;
            }
            .display_box:hover
            {
                background-color:#284761;
                color:#FFF;
            }

        </style>

    </head>

    <body>

        <!-- Wrapper -->
        <div class="wrapper push-wrapper">

            <!-- Header -->
            <header id="header">

                <!-- Top Bar -->
                <div class="topbar">
                    <div class="container header-cont">

                        <div class="col-md-4 logo">
                            <a style="display: inline-flex;" href="index.php"><img src="img/logo.png" class="img-logo"><H1 class="LogoText">IT </H1><H1 style="color:orange !important;    font-size: 29px;margin-top: 16px;" class="LogoText">LMS </H1></a>
                        </div>
                        <div class="col-md-8 ">
                            <ul class="options">
                                <li class="option">
                                    <a href="index.php" style="color:#FFFFFF">
                                        <i class="fa fa-home"></i>&nbsp;Home</a>
                                </li>
                                <li class="option">
                                    <a href="about.php" style="color:#FFFFFF">
                                        <i class="fa fa fa-files-o"></i>&nbsp;About</a>
                                </li>
                                <li class="option">
                                    <a href="login.php" style="color:#FFFFFF">
                                        <i class="fa fa-sign-in"></i>&nbsp;SignIn</a>
                                </li>
                                <li class="option">
                                    <a  href="signup.php"style="color:#FFFFFF">
                                        <i class="fa fa-sign-out"></i></span>&nbsp;signUp</a>
                                </li>
                            </ul>

                        </div>

                    </div>
                </div>
                <!-- Top Bar -->


            </header>
            <!-- Header -->

            <!-- Main Content -->
            <main class="main-content" style="background:#fff">



                <!-- Book Collections -->
                <section class="book-collection">
                    <div class="container">
                        <div class="row">

                            <!-- Book Collections Tabs -->
                            <div id="book-collections-tabs">

                                <!-- collection Name -->
                                <div class="col-lg-3 col-sm-12">

                                    <div class="panel panel-danger">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Admin Feature</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ul style="list-style-type: square;">
                                                <li>Admin</li>
                                                <li><a>Manage Books Category</a></li>
                                                <li><a>Manage Books</a></li>
                                                <li><a>Manage Librarian</a></li>
                                                <li><a>Manage Student</a></li>
                                                <li><a>Manage Booked Book</a></li>
                                                <li><a>Manage Return Book</a></li>

                                            </ul>
                                        </div>
                                    </div>


                                    <div class="panel panel-danger">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Librarian Feature</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ul style="list-style-type: square;">
                                                <li><a>Manage Books Category</a></li>
                                                <li><a>Manage Books</a></li>
                                                <li><a>Manage Student</a></li>
                                                <li><a>Booked Book</a></li>
                                                <li><a>Return Book</a></li>
                                                <li><a>Send Notification to Student</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel panel-danger">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Student Feature</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ul style="list-style-type: square;">
                                                <li><a>Student Register</a></li>
                                                <li><a>Student Login</a></li>
                                                <li><a>Search Book</a></li>
                                                <li><a>Issue Book</a></li>
                                                <li><a>Return Book</a></li>
                                                <li><a>Check Notification</a></li>
                                                <li><a>Update Profile</a></li>
                                                <li><a>Return Password</a></li>
                                            </ul>
                                        </div>
                                    </div>



                                </div>
                                <!-- collection Name -->

                                <!-- Collection Content -->
                                <div class="col-lg-9 col-sm-12">
                                    <div class="collection">

                                        <div class="sec-heading">
                                            <h3> <span class="theme-color">Books</span></h3>

                                        </div>
                                        <div id="search-results" class = "container">
                                            <center>
                                                <div style="margin:100px" class = "col-lg-8">
                                                    <input style="border-radius: 12px;outline: none;color: black;" name="book-name" id="book-name" type="text" autofocus />
                                                    <button onclick="Search()" style="margin:20px" type="button" class="btn btn-info">Search</button>
                                                </div>
                                            </center>
                                        </div>
                                        <script>
                                            function Search() {
                                                var search = $("#book-name").val();
                                                var data = "search=" + search;
												
					if(search.length >=3){
                                                $.ajax({
                                                    type: "POST",
                                                    url: "book-search.php",
                                                    data: data,
                                                    success: function (dataString) {
                                                        $('#search-results').html(dataString);
                                                    }
                                                });
												}else{
						alert("Keyword should have minimum 3 characters")
					}
                                            }
                                        </script>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </section>
                <!-- Book Collections --> 



                <br/>





            </main>
            <!-- Main Content -->

            <!-- Footer -->
            <footer id="footer"> 



                <!-- Sub Footer -->
                <div class="sub-foorer">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <p>Copyright <i class="fa fa-copyright"></i> 2018 <span class="theme-color"></span> All Rights Reserved.</p>
                            </div>
                            <div class="col-sm-6">
                                <a class="back-top" href="#">Back to Top<i class="fa fa-caret-up"></i></a>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Sub Footer -->

            </footer>
            <!-- Footer -->

        </div>

    </body>

</html>