<?php
include_once 'session.php';

if (isset($_POST['action'])) {

    switch ($_POST['action']) {
        case "search-book":
            ?>
            <script>
                $('#li-dashboard').attr('class', '');
                $('#li-book-category').attr('class', '');
                $('#li-books').attr('class', '');
                $('#li-books-stock').attr('class', '');
                $('#li-librarian').attr('class', '');
                $('#li-student').attr('class', '');
                $('#li-faculty').attr('class', '');
                $('#li-search-book').attr('class', 'active');
            </script>
            <div class = "container">
                <center>
                    <div style="margin:100px" class = "col-lg-8">
                        <input style="border-radius: 12px;outline: none;color: black;" name="book-name" id="book-name" type="text" autofocus />
                        <button onclick="Search()" style="margin:20px" type="button" class="btn btn-info">Search</button>
                    </div>
                </center>
            </div>
            <script>
                function Search() {
                    var search = $("#book-name").val();
                    var data = "search=" + search;
                    //alert(search);
					if(search.length >=5){
                    $.ajax({
                        type: "POST",
                        url: "book-search.php",
                        data: data,
                        success: function (dataString) {
                            //alert(dataString);
                            $('#sub-dashboard').html(dataString);
                        }
                    });
					}else{
						alert("Keyword should have minimum 5 characters");
					}
                }
            </script>
            <?php
            break;
        case "faculty":
            $Faculty = runQuery("SELECT * FROM `faculty`");
            ?>
            <script>
                $('#li-dashboard').attr('class', '');
                $('#li-book-category').attr('class', '');
                $('#li-books').attr('class', '');
                $('#li-books-stock').attr('class', '');
                $('#li-librarian').attr('class', '');
                $('#li-student').attr('class', '');
                $('#li-faculty').attr('class', 'active');
                $('#li-search-book').attr('class', '');
                function ShowFacultyReg() {
                    $('.faculty-reg').hide();
                    $('.faculty-table').hide();
                    $('.faculty-reg-form').show();
                    $('.faculty-reg-form').attr('style', 'display:inline-grid !important;');
                }

                function FacultyReg() {
                    var id = $('#id').val();
                    var name = $('#name').val();
                    var email = $('#email').val();
                    var mobile = $('#mobile').val();
                    var password = $('#password').val();
                    var post = $('#designation').val();
                    var quali = $('#qualification').val();
                    var dept = $('#dept').val();
                    var data = "member=faculty&id=" + id + "&name=" + name + "&email=" + email + "&mobile=" + mobile + "&password=" + password + "&post=" + post + "&qualification=" + quali + "&dept=" + dept;
                    //alert(data);
                    $.ajax({
                        type: "POST",
                        url: "newRegister.php",
                        data: data,
                        success: function (dataString123)
                        {
                            $('.faculty-reg').show();
                            $('.faculty-table').show();
                            $('.faculty-reg-form').hide();
                            $('.faculty-reg-form').attr('style', 'display:none !important;');
                        }
                    });
                }
            </script>
            <div class = "container noScroll">
                <center>
                    <h4 id="message-request"></h4>
                    <div class="row faculty-reg">


                        <div class="col-md-12">
                            <button onclick="ShowFacultyReg()" style="margin:20px" type="button" class="btn btn-info">Register New Faculty</button>
                        </div>

                        <div style="margin:50px" class = "col-lg-8 faculty-table">
                            <table class="table-responsive" border="1px">
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Faculty Id</th>
                                    <th>Department</th>
                                    <th>Qualification</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Action</th>
                                </tr>
                                <?php
								$_SESSION['DeleteHash'] = token(10);
                                foreach ($Faculty as $key => $value) {
                                    ?>
                                    <tr id="tr_<?php echo $Faculty[$key]['faculty_id']; ?>">
                                        <td><?php echo $key + 1; ?></td>
                                        <td><?php echo $Faculty[$key]['name'] . "-" . $Faculty[$key]['post']; ?></td>
                                        <td><?php echo $Faculty[$key]['faculty_id']; ?></td>
                                        <td><?php echo $Faculty[$key]['dept']; ?></td>
                                        <td><?php echo $Faculty[$key]['qualification']; ?></td>
                                        <td><?php echo $Faculty[$key]['email']; ?></td>
                                        <td><?php echo $Faculty[$key]['mobile']; ?></td>
                                        <td><i onclick="deleteFaculty('<?php echo $Faculty[$key]['faculty_id']; ?>','<?php echo $_SESSION['DeleteHash']; ?>')" class="fa fa-trash-o"></i></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
							<div id="ajax_update">
							</div>
							<script>
							function deleteFaculty(id,hash){
								var data = "id="+id+"&hash="+hash+"&action=faculty";
								//alert(data);
								$.ajax({
                        type: "POST",
                        url: "DelUser.php",
                        data: data,
                        success: function (dataString123)
                        {
							alert("Deleted Sucessfully");
							$('#ajax_update').html(dataString123);
                        }
                    });
							}
							</script>
                        </div>
                    </div>
                </center>
                <div style="display:none;" class="container faculty-reg-form">
                    <div>
                        <label style="width:100px">Id No:</label>
                        <input class="text-input" name="id" type="text" id="id" required>
                    </div>
                    <div>
                        <label style="width:100px">Name:</label>
                        <input class="text-input" name="name" type="text" id="name" required>
                    </div>
                    <div>
                        <label style="width:100px">Email Id:</label>
                        <input class="text-input" name="email" type="text" id="email" required>
                    </div>
                    <div>
                        <label style="width:100px">Mobile No:</label>
                        <input class="text-input" name="mobile" type="text" id="mobile" required>
                    </div>
                    <div>
                        <label style="width:100px">Designation:</label>
                        <input class="text-input" name="designation" type="text" id="designation" required>
                    </div>
                    <div>
                        <label style="width:100px">Department:</label>
                        <input class="text-input" name="department" type="text" id="dept" required>
                    </div>
                    <div>
                        <label style="width:100px">Qualification:</label>
                        <input class="text-input" name="qualification" type="text" id="qualification" required>
                    </div>
                    <div>
                        <label style="width:100px">Password:</label>
                        <input class="text-input" name="password" type="password" id="password" required>
                    </div>
                    <div>
                        <button onclick="FacultyReg()" style="margin:20px;width:150px;margin-left: 135px;" type="button" class="btn btn-warning">Register</button>
                    </div>
                </div>
                <style>
                    .text-input{
                        width: 300px !important;
                        margin: 10px !important;
                        height: 35px !important;
                        border: 1px solid #003973 !important;
                        border-radius: 10px !important;
                        color:black !important;
                    }
                    .table-responsive {
                        max-height:300px;
                    }
                    body{
                        overflow: hidden !important;
                    }
                </style>
            </div>
            <?php
            break;
        case "student":
            $Student = runQuery("SELECT * FROM `student`");
            ?>
            <script>
                $('#li-dashboard').attr('class', '');
                $('#li-book-category').attr('class', '');
                $('#li-books').attr('class', '');
                $('#li-books-stock').attr('class', '');
                $('#li-librarian').attr('class', '');
                $('#li-student').attr('class', 'active');
                $('#li-faculty').attr('class', '');
                $('#li-search-book').attr('class', '');
            </script>
            <div class = "container">

                <center>
                    <div style="margin:50px" class = "col-lg-8 ">
                        <table border="1px">
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Student Id</th>
                                <th>Branch</th>
                                <th>Mobile</th>
                                <th>Action</th>
                            </tr>
                            <?php
							$_SESSION['DeleteHash'] = token(10);
                            foreach ($Student as $key => $value) {
                                ?>
                                <tr id="tr_<?php echo $Student[$key]['student_id']; ?>">
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $Student[$key]['name']; ?></td>
                                    <td><?php echo $Student[$key]['student_id']; ?></td>
                                    <td><?php echo $Student[$key]['branch']; ?></td>
                                    <td><?php echo $Student[$key]['mobile']; ?></td>
									<td><i onclick="deleteStudent('<?php echo $Student[$key]['student_id']; ?>','<?php echo $_SESSION['DeleteHash']; ?>')" class="fa fa-trash-o"></i></td>

                                </tr>
                                <?php
                            }
                            ?>
                        </table>
						<div id="ajax_update_student">
							</div>
							<script>
							function deleteStudent(id,hash){
								var data = "id="+id+"&hash="+hash+"&action=student";
								//alert(data);
								$.ajax({
                        type: "POST",
                        url: "DelUser.php",
                        data: data,
                        success: function (dataString123)
                        {
							alert("Deleted Sucessfully");
							$('#ajax_update_student').html(dataString123);
                        }
                    });
							}
							</script>
                    </div>
                </center>
            </div>
            <?php
            break;
        case "librarian":
            $Admin = runQuery("SELECT * FROM `admin`");
            ?>

            <script>
                $('#li-dashboard').attr('class', '');
                $('#li-book-category').attr('class', '');
                $('#li-books').attr('class', '');
                $('#li-books-stock').attr('class', '');
                $('#li-librarian').attr('class', 'active');
                $('#li-student').attr('class', '');
                $('#li-faculty').attr('class', '');
                $('#li-search-book').attr('class', '');
                function ShowAdminReg() {
                    $('.admin-reg').hide();
                    $('.admin-table').hide();
                    $('.admin-reg-form').show();
                    $('.admin-reg-form').attr('style', 'display:inline-grid !important;');
                }

                function AdminReg() {
                    var id = $('#id').val();
                    var name = $('#name').val();
                    var email = $('#email').val();
                    var mobile = $('#mobile').val();
                    var password = $('#password').val();
                    var data = "member=librarian&id=" + id + "&name=" + name + "&email=" + email + "&mobile=" + mobile + "&password=" + password;
                    //alert(data);
                    $.ajax({
                        type: "POST",
                        url: "newRegister.php",
                        data: data,
                        success: function (dataString123) {
                            //alert(dataString123);
                            $('.admin-reg').show();
                            $('.admin-table').show();
                            $('.admin-reg-form').hide();
                            $('.admin-reg-form').attr('style', 'display:none !important;');
                        }
                    });
                }

            </script>

            <div class = "container">
                <div class="row admin-reg">
                    <div class="col-md-6">
                        <h4 id="message-request"></h4>
                    </div>
                    <div class="col-md-6">
                        <button onclick="ShowAdminReg()" style="margin:20px" type="button" class="btn btn-info">Register New Librarian</button>
                    </div>
                </div>
                <center>
                    <div style="margin:50px" class = "col-lg-8 admin-table">
                        <table border="1px">
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Librarian Id</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            <?php
							$_SESSION['DeleteHash'] = token(10);
                            foreach ($Admin as $key => $value) {
                                ?>
                                <tr id="tr_<?php echo $Admin[$key]['id']; ?>">
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $Admin[$key]['name']; ?></td>
                                    <td><?php echo $Admin[$key]['id']; ?></td>
                                    <td><?php echo $Admin[$key]['mobile']; ?></td>
                                    <td><?php echo $Admin[$key]['email']; ?></td>
									<td><i onclick="deleteAdmin('<?php echo $Admin[$key]['id'];; ?>','<?php echo $_SESSION['DeleteHash']; ?>')" class="fa fa-trash-o"></i></td>

                                </tr>
                                <?php
                            }
                            ?>
                        </table>
						<div id="ajax_update_admin">
							</div>
							<script>
							function deleteAdmin(id,hash){
								var data = "id="+id+"&hash="+hash+"&action=admin";
								//alert(data);
								$.ajax({
                        type: "POST",
                        url: "DelUser.php",
                        data: data,
                        success: function (dataString123)
                        {
							alert("Deleted Sucessfully");
							$('#ajax_update_admin').html(dataString123);
                        }
                    });
							}
							</script>
                    </div>
                </center>
                <div style="display:none;" class="container admin-reg-form">
                    <div>
                        <label style="width:100px">Id No:</label>
                        <input class="text-input" name="id" type="text" id="id" required>
                    </div>
                    <div>
                        <label style="width:100px">Name:</label>
                        <input class="text-input" name="name" type="text" id="name" required>
                    </div>
                    <div>
                        <label style="width:100px">Email Id:</label>
                        <input class="text-input" name="email" type="text" id="email" required>
                    </div>
                    <div>
                        <label style="width:100px">Mobile No:</label>
                        <input class="text-input" name="mobile" type="text" id="mobile" required>
                    </div>
                    <div>
                        <label style="width:100px">Password:</label>
                        <input class="text-input" name="password" type="password" id="password" required>
                    </div>
                    <div>
                        <button onclick="AdminReg()" style="margin:20px;width:150px;margin-left: 135px;" type="button" class="btn btn-warning">Register</button>
                    </div>
                </div>
                <style>
                    .text-input{
                        width: 300px !important;
                        margin: 10px !important;
                        height: 35px !important;
                        border: 1px solid #003973 !important;
                        border-radius: 10px !important;
                        color:black  !important;
                    }
                </style>
            </div>
            <?php
            break;
        case "books-stock":
            $Books = runQuery("SELECT * FROM `books`");
            ?>
            <div class = "container">
                <center>
                    <div style="margin:50px" class = "col-lg-8">
                        <table border="1px">
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Author</th>
                                <th>Availability</th>
                            </tr>
                            <?php
                            foreach ($Books as $key => $value) {
                                ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $Books[$key]['book_name']; ?></td>
                                    <td><?php echo $Books[$key]['category']; ?></td>
                                    <td><?php echo $Books[$key]['author']; ?></td>
                                    <td><?php echo $Books[$key]['count']; ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                </center>
            </div>
            <script>
                $('#li-dashboard').attr('class', '');
                $('#li-book-category').attr('class', '');
                $('#li-books').attr('class', '');
                $('#li-books-stock').attr('class', 'active');
                $('#li-librarian').attr('class', '');
                $('#li-student').attr('class', '');
                $('#li-faculty').attr('class', '');
                $('#li-search-book').attr('class', '');
            </script>
            <?php
            break;
        case "books":
            $Books = runQuery("SELECT * FROM `books`");
            ?>
            <script>
                $('#li-dashboard').attr('class', '');
                $('#li-book-category').attr('class', '');
                $('#li-books').attr('class', 'active');
                $('#li-books-stock').attr('class', '');
                $('#li-librarian').attr('class', '');
                $('#li-student').attr('class', '');
                $('#li-faculty').attr('class', '');
                $('#li-search-book').attr('class', '');
            </script>
            <div class = "container">
                <center>
                    <div style="margin:50px" class = "col-lg-8">
                        <table border="1px">
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Author</th>
                                <th>Publisher</th>
                                <th>Pdf</th>
                            </tr>
                            <?php
                            foreach ($Books as $key => $value) {
                                ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $Books[$key]['book_name']; ?></td>
                                    <td><?php echo $Books[$key]['category']; ?></td>
                                    <td><?php echo $Books[$key]['author']; ?></td>
                                    <td><?php echo $Books[$key]['publisher']; ?></td>
                                    <td><a href="<?php echo $Books[$key]['url']; ?>" target="_blank"><?php echo $Books[$key]['book_name'] . "By" . $Books[$key]['author']; ?></a></td>

                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                </center>
            </div>

            <?php
            break;
        case "book-category":
            ?>
            <script>
                $('#li-dashboard').attr('class', '');
                $('#li-book-category').attr('class', 'active');
                $('#li-books').attr('class', '');
                $('#li-books-stock').attr('class', '');
                $('#li-librarian').attr('class', '');
                $('#li-student').attr('class', '');
                $('#li-faculty').attr('class', '');
                $('#li-search-book').attr('class', '');
            </script>
            <?php
            break;
        case "dashboard":
            $StudentCount = runQuery("SELECT count(*) FROM `student`");
            $FacultyCount = runQuery("SELECT count(*) FROM `faculty`");
            $LibrarianCount = runQuery("SELECT count(*) FROM `admin`");
            ?>
            <div class="container">
                <center>
                    <div class="col-lg-8">
                        <h3 class="btn btn-warning" style="padding:20px;width:150px;margin:30px">Librarian Count : <?php echo $LibrarianCount[0]['count(*)']; ?></h3>
                        <h3 class="btn btn-warning" style="padding:20px;width:150px;margin:30px">Faculty Count : <?php echo $FacultyCount[0]['count(*)']; ?></h3>
                        <h3 class="btn btn-warning" style="padding:20px;width:150px;margin:30px">Students Count : <?php echo $StudentCount[0]['count(*)']; ?></h3>
                </center>
            </div>
            <script>
                $('#li-dashboard').attr('class', 'active');
                $('#li-book-category').attr('class', '');
                $('#li-books').attr('class', '');
                $('#li-books-stock').attr('class', '');
                $('#li-librarian').attr('class', '');
                $('#li-student').attr('class', '');
                $('#li-faculty').attr('class', '');
                $('#li-search-book').attr('class', '');
            </script>
            <?php
            break;
        default:
    }
}
?>